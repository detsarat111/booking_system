<?php

namespace App\Http\Controllers;

use App\Models\close_shift;
use App\Models\paymentMethod;
use App\Models\payment_amount;
use App\Models\pos_sale;
use App\Models\prescriptions;
use App\Models\productbom;
use App\Models\productinsale;
use App\Models\product_vavaincode_view;
use App\Models\Sale;
use App\Models\Saleline;
use App\Models\Serail;
use App\Models\diagnosis_list;
use App\Models\stocktransaction;
use App\Models\laboratory;
use App\Models\stockkeeping;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function PaymenPos(Request $request)
    {
        //return $request->all();
        $curencycode = 'USD';
        $mydate = date('Y-m-d');
        $pos_sale = pos_sale::where('created_by', '=', $request->created_by)->where('statue', '=', 'open')->get();
            $close_shift = close_shift::where('created_by', '=', $request->created_by)->where('statue', '=', 'open')->first();
            if ($close_shift) {
                $spid = $close_shift->id;
                if ($request->type == 'pos') {
                    if ($request->created_by != '' && count($pos_sale) > 0) {
                    $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
                    $Prifixcode = $serail_no->prefix_code;
                    $Code_qty = $serail_no->qty_code;
                    $Sart_at = $serail_no->start_code;
                    $End_code = $serail_no->end_code;
                    $newCode = (int) $Sart_at + (int) $End_code;
                    $serail_no = $Prifixcode;
                    for ($i = 0; $i < ((int) $Code_qty - strlen($newCode)); $i++) {
                        $serail_no = $serail_no . "0";
                    }
                    $serail_no = $serail_no . $newCode;
                    $product = prescriptions::create([
                        'preid' => $serail_no,
                        'pateinid' => 'Genneral',
                        'created_by' => $request['created_by'],
                        'status' => 'success',
                        'age' => $request->age,
                        'width' => $request->weight,
                        'group_code' => $request->group_code,
                        'visit_date' => $mydate,
                        'type' => $request['type'],
                    ]);
                    if ($product) {
                        if ($product) {
                            $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                            $serilano->end_code = $newCode;
                            $serilano->save();
                            if ($serilano) {
                                $totalusd = floatval($request->totalpay);
                                $totalkh = floatval($request->totalpay * $request->exchange_rate);
                                $Sale = Sale::create([
                                    'document_no' => $serail_no,
                                    "document_type" => 'pos sale invoice',
                                    'closeshift_id' => $spid,
                                    'description' => 'Product sale pos on ' . $mydate,
                                    'total_amount' => $totalkh,
                                    'total_amount_usd' => $totalusd,
                                    'amoun' => $request->amount,
                                    'change_amount' => $request->changeKH,
                                    'paymeny_method' => $request->paymeny_method,
                                    'change_amount_usd' => $request->changeUS,
                                    'total_receipts' => $request->payTwo,
                                    'total_receipt_usd' => $request->payOne,
                                    'exchane_rate' => $request->exchange_rate,
                                    'type' => 'pos',
                                    'discount' => $request->discount,
                                    'curency_code' => 'USD',
                                    'statue' => 'open',
                                    'created_by' => $request->created_by,
                                ]);
                                if ($Sale) {
                                    foreach ($pos_sale as $item) {
                                        $curency_code = $item->curency_code;
                                        $Saleline = Saleline::create([
                                            'document_no' => $serail_no,
                                            "document_type" => 'pos sale invoice',
                                            "product_no" => $item->product_no,
                                            "description" => $item->description,
                                            "unit_code" => $item->unit_code,
                                            "unit_price" => $item->unit_price,
                                            "inventory" => $item->inventory,
                                            "total_amount" => $item->total_amount,
                                            "curency_code" => $item->curency_code,
                                            'created_by' => $request['created_by'],
                                            'curency_code' => 'USD',
                                            "remark" => $item->remark,
                                        ]);
                                        if ($Saleline) {
                                            $stocktransaction = stocktransaction::create([
                                                'document_no' => $serail_no,
                                                "document_type" => 'income',
                                                'product_no' =>  $item->product_no,
                                                'description' => $item->description,
                                                'unit_of_measure_code' => $item->unit_code,
                                                'unit_price' => $item->unit_price,
                                                'inventory' =>  $item->inventory,
                                                'total_amount' => $item->total_amount,
                                                'curency_code' => $item->curency_code,
                                                'remark' => $item->remark,
                                                'created_by' => $request['created_by'],
                                            ]);
                                        }
                                        if ($stocktransaction) {
                                            $productbom = productbom::where('product_no', '=', $item->product_no)->get();
                                            if (count($productbom) > 0) {
                                                foreach ($productbom as $bom) {
                                                    $unitcode = $bom->bom_unit_of_measure_code;
                                                    $quantity = $bom->quantity;
                                                    $productvariantcode = product_vavaincode_view::where('product_no', '=', $bom->boom_product_id)
                                                        ->where('variant_unit_of_measure_code', '=', $bom->bom_unit_of_measure_code)
                                                        ->get();
                                                    if (count($productvariantcode) > 0) {
                                                        foreach ($productvariantcode as $variantcode) {
                                                            $varunit = $variantcode->stock_unit_of_measure_code;
                                                            $varquantity = $variantcode->quantity_per_unit;
                                                            $inventory = doubleval($quantity) * doubleval($varquantity);


                                                                do {
                                                                    try {
                                                                        $stockkeeping = stockkeeping::orderBy('exprit_date', 'ASC')->where('product_no', '=', $bom->boom_product_id)->where('statuse', '=', 'open')->first();
                                                                        $stinventory = $stockkeeping->inventory;
                                                                        if (doubleval($stinventory) > doubleval($inventory)) {
                                                                            $product = productinsale::create([
                                                                                'document_no' => $serail_no,
                                                                                'boom_product_id' => $bom->boom_product_id,
                                                                                'product_no' => $bom->product_no,
                                                                                'description' => $bom->description,
                                                                                'unit_code' => $varunit,
                                                                                'stock_id'=>$stockkeeping->id,
                                                                                'inventory' => $inventory,
                                                                                'per_unit' => $bom->quantity_per_unit,
                                                                                'created_by' => $request['created_by'],
                                                                            ]);
                                                                            $stinventory = doubleval($stinventory) - doubleval($inventory);
                                                                            $stockkeeping->update(['inventory' => $stinventory]);
                                                                            $inventory = 0;
                                                                        } else {
                                                                            $product = productinsale::create([
                                                                                'document_no' => $serail_no,
                                                                                'boom_product_id' => $bom->boom_product_id,
                                                                                'product_no' => $bom->product_no,
                                                                                'description' => $bom->description,
                                                                                'unit_code' => $varunit,
                                                                                'stock_id'=>$stockkeeping->id,
                                                                                'inventory' => $stinventory,
                                                                                'per_unit' => $bom->quantity_per_unit,
                                                                                'created_by' => $request['created_by'],
                                                                            ]);
                                                                            $stinventory = doubleval($inventory) - doubleval($stinventory);
                                                                            $inventory = $stinventory;
                                                                            $stockkeeping->update(['inventory' => 0, 'statuse' => 'close']);
                                                                        }
                                                                    } catch (Exception $e) {}

                                                                } while ($inventory > 0);

                                                        }
                                                    }

                                                }
                                            } else {
                                                $productvariantcode = product_vavaincode_view::where('product_no', '=', $item->product_no)
                                                    ->where('variant_unit_of_measure_code', '=', $item->unit_code)
                                                    ->get();
                                                foreach ($productvariantcode as $variantcode) {
                                                    $varunit = $variantcode->stock_unit_of_measure_code;
                                                    $varquantity = $variantcode->quantity_per_unit;
                                                    $inventory = doubleval($item->inventory) * doubleval($varquantity);


                                                        do {
                                                            try {
                                                                $stockkeeping = stockkeeping::orderBy('exprit_date', 'ASC')->where('product_no', '=', $item->product_no)->where('statuse', '=', 'open')->first();
                                                                $stinventory = $stockkeeping->inventory;
                                                                if (doubleval($stinventory) > doubleval($inventory)) {
                                                                    $product = productinsale::create([
                                                                        'document_no' => $serail_no,
                                                                        'boom_product_id' => $item->product_no,
                                                                        'product_no' => $item->product_no,
                                                                        'description' => $item->description,
                                                                        'unit_code' => $varunit,
                                                                        'inventory' => $inventory,
                                                                        'stock_id'=>$stockkeeping->id,
                                                                        'per_unit' => '1',
                                                                        'created_by' => $request['created_by'],
                                                                    ]);
                                                                    $stinventory = doubleval($stinventory) - doubleval($inventory);
                                                                    $stockkeeping->update(['inventory' => $stinventory]);
                                                                    $inventory = 0;
                                                                } else {
                                                                    $product = productinsale::create([
                                                                        'document_no' => $serail_no,
                                                                        'boom_product_id' => $item->product_no,
                                                                        'product_no' => $item->product_no,
                                                                        'description' => $item->description,
                                                                        'unit_code' => $varunit,
                                                                        'inventory' => $stinventory,
                                                                        'stock_id'=>$stockkeeping->id,
                                                                        'per_unit' => '1',
                                                                        'created_by' => $request['created_by'],
                                                                    ]);
                                                                    $stinventory = doubleval($inventory) - doubleval($stinventory);
                                                                    $inventory = $stinventory;
                                                                    $stockkeeping->update(['inventory' => 0, 'statuse' => 'close']);
                                                                }
                                                            } catch (Exception $e) {}
                                                        } while ($inventory > 0);

                                                }
                                            }
                                        }
                                    }
                                    foreach ($pos_sale as $element) {
                                        $element->delete();
                                    }
                                    if($product){
                                        $payment_amount = payment_amount::create([
                                            'document_no' => $serail_no,
                                            'document' => 'pos sale product Invoince',
                                            'document_type' => 'income',
                                            'decription' => 'pos sale Invoince' . $serail_no,
                                            'totale_balanec' => $request->amount,
                                            'exchane_rate' =>   $request['exchange_rate'],
                                            'currency_code' =>  $curency_code,
                                            'paymant_amount' => $request->amount,
                                            'statue' => 'open',
                                            'created_by' => $request['created_by'],
                                        ]);
                                    }
                                    if( $payment_amount){
                                        $Sale = Sale::where('document_no', '=', $serail_no)->first();
                                        $Saleline = Saleline::where('document_no', '=', $serail_no)->get();
                                        return ['statue' => true, 'Sale' => $Sale,'Saleline'=>$Saleline];
                                    }
                                }
                            } else {
                                return ['statue' => false];
                            }
                        } else {
                            return ['statue' => false];
                        }

                        } else {
                        return ['statue' => false];
                    }
                    } else {
                     return ['statue' => false];
                }

            } else  if ($request->type == 'prescription') {
               // return $request ->all();
                $prescriptions = prescriptions::where('created_by', '=', $request->created_by)->where('preid', '=', $request->preid)->first();
                $diagnosislist = diagnosis_list::where('created_by', '=', $request->created_by)->where('preid', '=', $request->preid)->get();
                if($prescriptions && $diagnosislist){
                    $serail_no = $prescriptions->preid;
                    $Sale = Sale::create([
                                    'document_no' => $serail_no,
                                    "document_type" => 'prescription sale invoice',
                                    'closeshift_id' => $spid,
                                    'description' => 'Product sale prescription on ' . $mydate,
                                    'total_amount' => $request->pay_amount,
                                    'total_amount_usd' => $request->usd_pay_amount,
                                    'amoun' => $request->total_amount,
                                    'change_amount' => $request->change_riel,
                                    'paymeny_method' => $request->paymentmethodselect,
                                    'change_amount_usd' => $request->change_usd,
                                    'total_receipts' => $request->pay_riel,
                                    'total_receipt_usd' => $request->pay_usd,
                                    'exchane_rate' => $request->ExchangeRate,
                                    'type' => 'prescription',
                                    'discount' => $request->discount,
                                    'curency_code' => 'USD',
                                    'statue' => 'open',
                                    'created_by' => $request->created_by,
                                ]);
                                if ($Sale) {
                                    foreach ($diagnosislist as $item) {
                                        $curency_code = $item->curency_code;
                                        $varquantity = $item->qty;
                                        $Saleline = Saleline::create([
                                            'document_no' => $serail_no,
                                            "document_type" => 'prescription sale invoice',
                                            "product_no" => $item->product_no,
                                            "description" => $item->description,
                                            "unit_code" => $item->unit,
                                            "unit_price" => $item->unit_price,
                                            "inventory" => $item->qty,
                                            "total_amount" => $item->amount,
                                            'curency_code' => 'USD',
                                            'created_by' => $request['created_by'],
                                            "remark" => $item->remark,
                                        ]);
                                        if ($Saleline) {
                                            $stocktransaction = stocktransaction::create([
                                                'document_no' => $serail_no,
                                                "document_type" => 'income',
                                                'product_no' =>  $item->product_no,
                                                'description' => $item->description,
                                                'unit_of_measure_code' => $item->unit,
                                                'unit_price' => $item->unit_price,
                                                'inventory' =>  $item->qty,
                                                'total_amount' => $request->total_amount,
                                                'curency_code' => 'USD',
                                                'remark' =>  $mydate,
                                                'created_by' => $request['created_by'],
                                            ]);
                                        }
                                        if ($stocktransaction) {
                                            $productbom = productbom::where('product_no', '=', $item->product_no)->get();
                                            if (count($productbom) > 0) {
                                                foreach ($productbom as $bom) {
                                                    $unitcode = $bom->bom_unit_of_measure_code;
                                                    $quantity = $bom->quantity;
                                                    $productvariantcode = product_vavaincode_view::where('product_no', '=', $bom->boom_product_id)
                                                        ->where('variant_unit_of_measure_code', '=', $bom->bom_unit_of_measure_code)
                                                        ->get();
                                                    if (count($productvariantcode) > 0) {
                                                        foreach ($productvariantcode as $variantcode) {
                                                            $varunit = $variantcode->stock_unit_of_measure_code;
                                                            $varquantity = $variantcode->quantity_per_unit;
                                                            $inventory = doubleval($quantity) * doubleval($varquantity)*doubleval($item->qty);

                                                                do {
                                                                    try {
                                                                        $stockkeeping = stockkeeping::orderBy('exprit_date', 'ASC')->where('product_no', '=', $bom->boom_product_id)->where('statuse', '=', 'open')->first();
                                                                        $stinventory = $stockkeeping->inventory;
                                                                        if (doubleval($stinventory) > doubleval($inventory)) {
                                                                            $product = productinsale::create([
                                                                                'document_no' => $serail_no,
                                                                                'boom_product_id' => $bom->boom_product_id,
                                                                                'product_no' => $bom->product_no,
                                                                                'description' => $bom->description,
                                                                                'unit_code' => $varunit,
                                                                                'inventory' => $inventory,
                                                                                'stock_id'=>$stockkeeping->id,
                                                                                'per_unit' => $bom->quantity_per_unit,
                                                                                'created_by' => $request['created_by'],
                                                                            ]);
                                                                            $stinventory = doubleval($stinventory) - doubleval($inventory);
                                                                            $stockkeeping->update(['inventory' => $stinventory]);
                                                                            $inventory = 0;
                                                                        } else {
                                                                            $product = productinsale::create([
                                                                                'document_no' => $serail_no,
                                                                                'boom_product_id' => $bom->boom_product_id,
                                                                                'product_no' => $bom->product_no,
                                                                                'description' => $bom->description,
                                                                                'unit_code' => $varunit,
                                                                                'inventory' => $stinventory,
                                                                                'stock_id'=>$stockkeeping->id,
                                                                                'per_unit' => $bom->quantity_per_unit,
                                                                                'created_by' => $request['created_by'],
                                                                            ]);

                                                                            $stinventory = doubleval($inventory) - doubleval($stinventory);
                                                                            $stockkeeping->update(['inventory' => 0, 'statuse' => 'close']);
                                                                            $inventory = $stinventory;
                                                                        }
                                                                    } catch (Exception $e) {}

                                                                } while ($inventory > 0);

                                                        }
                                                    }

                                                }
                                            } else {
                                                $productvariantcode = product_vavaincode_view::where('product_no', '=', $item->product_no)
                                                    ->where('variant_unit_of_measure_code', '=', $item->unit)
                                                    ->get();
                                                foreach ($productvariantcode as $variantcode) {
                                                    $varunit = $variantcode->stock_unit_of_measure_code;
                                                    $varquantity = $variantcode->quantity_per_unit;
                                                    $inventory = doubleval($item->qty) * doubleval($varquantity);


                                                        do {
                                                            try {
                                                                $stockkeeping = stockkeeping::orderBy('exprit_date', 'ASC')->where('product_no', '=', $item->product_no)->where('statuse', '=', 'open')->first();
                                                                $stinventory = $stockkeeping->inventory;
                                                                if (doubleval($stinventory) > doubleval($inventory)) {
                                                                    $product = productinsale::create([
                                                                        'document_no' => $serail_no,
                                                                        'boom_product_id' => $item->product_no,
                                                                        'product_no' => $item->product_no,
                                                                        'description' => $item->description,
                                                                        'unit_code' => $varunit,
                                                                        'inventory' => $inventory,
                                                                        'per_unit' => '1',
                                                                        'stock_id'=>$stockkeeping->id,
                                                                        'created_by' => $request['created_by'],
                                                                    ]);
                                                                    $stinventory = doubleval($stinventory) - doubleval($inventory);
                                                                    $stockkeeping->update(['inventory' => $stinventory]);
                                                                    $inventory = 0;
                                                                } else {
                                                                    $product = productinsale::create([
                                                                        'document_no' => $serail_no,
                                                                        'boom_product_id' => $item->product_no,
                                                                        'product_no' => $item->product_no,
                                                                        'description' => $item->description,
                                                                        'unit_code' => $varunit,
                                                                        'inventory' => $stinventory,
                                                                        'per_unit' => '1',
                                                                        'stock_id'=>$stockkeeping->id,
                                                                        'created_by' => $request['created_by'],
                                                                    ]);
                                                                    $stinventory = doubleval($inventory) - doubleval($stinventory);
                                                                    $inventory = $stinventory;
                                                                    $stockkeeping->update(['inventory' => 0, 'statuse' => 'close']);
                                                                }
                                                            } catch (Exception $e) {}
                                                        } while ($inventory > 0);

                                                }
                                            }
                                        }
                                    }
                                   if($prescriptions->appointment_date =='') {
                                        $prescriptions->status = "close";
                                        $prescriptions->save();
                                      }else{
                                       $prescriptions->status = "waiting";
                                       $prescriptions->save();
                                     }
                                    if($prescriptions){
                                        $payment_amount = payment_amount::create([
                                            'document_no' => $serail_no,
                                            'document' => 'prescription sale product Invoince',
                                            'document_type' => 'income',
                                            'decription' => 'prescription sale Invoince' . $serail_no,
                                            'totale_balanec' => $request->total_amount,
                                            'exchane_rate' =>   $request['ExchangeRate'],
                                            'currency_code' =>  $curency_code,
                                            'paymant_amount' => $request->total_amount,
                                            'statue' => 'open',
                                            'created_by' => $request['created_by'],
                                        ]);
                                    }
                                    if( $payment_amount){
                                        return ['statue' => true];
                                    }
                                }
                }  else {
                     return ['statue' => false];
                }
            } else if($request->type == 'labo'){
                $prescriptions = prescriptions::where('created_by', '=', $request->created_by)->where('preid', '=', $request->preid)->first();
                $laboratory = laboratory::where('created_by', '=', $request->created_by)->where('labId', '=', $request->preid)->get();
                if($prescriptions && $laboratory){
                    $serail_no = $prescriptions->preid;
                    $Sale = Sale::create([
                                    'document_no' => $serail_no,
                                    "document_type" => 'labo sale service invoice',
                                    'closeshift_id' => $spid,
                                    'description' => 'Product sale service labo on ' . $mydate,
                                    'total_amount' => $request->pay_amount,
                                    'total_amount_usd' => $request->usd_pay_amount,
                                    'amoun' => $request->total_amount,
                                    'change_amount' => $request->change_riel,
                                    'paymeny_method' => $request->paymentmethodselect,
                                    'change_amount_usd' => $request->change_usd,
                                    'total_receipts' => $request->pay_riel,
                                    'total_receipt_usd' => $request->pay_usd,
                                    'exchane_rate' => $request->ExchangeRate,
                                    'type' => 'labo',
                                    'discount' => $request->discount,
                                    'curency_code' => 'USD',
                                    'statue' => 'open',
                                    'created_by' => $request->created_by,
                                ]);
                                if ($Sale) {
                                    foreach ($laboratory as $item) {

                                        $varquantity = 1;
                                        $Saleline = Saleline::create([
                                            'document_no' => $serail_no,
                                            "document_type" => 'labo sale service invoice',
                                            "product_no" => $item->product_no,
                                            "description" => $item->description,
                                            "unit_code" => $item->unit,
                                            "unit_price" => $item->unit_price,
                                            "inventory" => $item->qty,
                                            "total_amount" => $item->amount,
                                            'curency_code' => 'USD',
                                            'created_by' => $request['created_by'],
                                            "remark" => $item->remark,
                                        ]);
                                        if ($Saleline) {
                                            $stocktransaction = stocktransaction::create([
                                                'document_no' => $serail_no,
                                                "document_type" => 'income',
                                                'product_no' =>  $item->product_no,
                                                'description' => $item->description,
                                                'unit_of_measure_code' => $item->unit,
                                                'unit_price' => $item->unit_price,
                                                'inventory' =>  $item->qty,
                                                'total_amount' => $request->total_amount,
                                                'curency_code' => 'USD',
                                                'remark' =>  $mydate,
                                                'created_by' => $request['created_by'],
                                            ]);
                                        }
                                        if ($stocktransaction) {
                                            $productbom = productbom::where('product_no', '=', $item->product_no)->get();

                                            if (count($productbom) > 0) {
                                                foreach ($productbom as $bom) {
                                                    $unitcode = $bom->bom_unit_of_measure_code;
                                                    $quantity = $bom->quantity;
                                                    $productvariantcode = product_vavaincode_view::where('product_no', '=', $bom->boom_product_id)
                                                        ->where('variant_unit_of_measure_code', '=', $bom->bom_unit_of_measure_code)
                                                        ->get();
                                                    if (count($productvariantcode) > 0) {
                                                        foreach ($productvariantcode as $variantcode) {
                                                            $varunit = $variantcode->stock_unit_of_measure_code;
                                                            $varquantity = $variantcode->quantity_per_unit;
                                                           // if($varquantity == null) $varquantity = 0;

                                                            $inventory = doubleval($quantity) * doubleval($varquantity);


                                                                do {
                                                                    try {
                                                                        $stockkeeping = stockkeeping::orderBy('exprit_date', 'ASC')->where('product_no', '=', $bom->boom_product_id)->where('statuse', '=', 'open')->first();
                                                                      //  return $stockkeeping;


                                                                        if(!$stockkeeping){
                                                                            $product = productinsale::create([
                                                                                'document_no' => $serail_no,
                                                                                'boom_product_id' => $bom->boom_product_id,
                                                                                'product_no' => $bom->product_no,
                                                                                'description' => $bom->description,
                                                                                'unit_code' => $varunit,
                                                                                'inventory' => $inventory,
                                                                                'stock_id'=>'-1',
                                                                                'per_unit' => $bom->quantity_per_unit,
                                                                                'created_by' => $request['created_by'],
                                                                            ]);
                                                                            $inventory = 0;
                                                                        }else{
                                                                            $stinventory = $stockkeeping->inventory;
                                                                            if(doubleval($stinventory) > doubleval($inventory)) {
                                                                                $product = productinsale::create([
                                                                                    'document_no' => $serail_no,
                                                                                    'boom_product_id' => $bom->boom_product_id,
                                                                                    'product_no' => $bom->product_no,
                                                                                    'description' => $bom->description,
                                                                                    'unit_code' => $varunit,
                                                                                    'inventory' => $inventory,
                                                                                    'stock_id'=>$stockkeeping->id,
                                                                                    'per_unit' => $bom->quantity_per_unit,
                                                                                    'created_by' => $request['created_by'],
                                                                                ]);
                                                                                $stinventory = doubleval($stinventory) - doubleval($inventory);
                                                                                $stockkeeping->update(['inventory' => $stinventory]);
                                                                                $inventory = 0;
                                                                            } else {
                                                                                $product = productinsale::create([
                                                                                    'document_no' => $serail_no,
                                                                                    'boom_product_id' => $bom->boom_product_id,
                                                                                    'product_no' => $bom->product_no,
                                                                                    'description' => $bom->description,
                                                                                    'unit_code' => $varunit,
                                                                                    'inventory' => $stinventory,
                                                                                    'stock_id'=>$stockkeeping->id,
                                                                                    'per_unit' => $bom->quantity_per_unit,
                                                                                    'created_by' => $request['created_by'],
                                                                                ]);
                                                                                $stinventory = doubleval($inventory) - doubleval($stinventory);
                                                                                $stockkeeping->update(['inventory' => 0, 'statuse' => 'close']);
                                                                            }
                                                                        }


                                                                    } catch (Exception $e) {}

                                                                } while ($inventory > 0);

                                                        }
                                                    }

                                                }
                                            } else {
                                                $productvariantcode = product_vavaincode_view::where('product_no', '=', $item->product_no)
                                                    ->where('variant_unit_of_measure_code', '=', $item->unit)
                                                    ->get();
                                                foreach ($productvariantcode as $variantcode) {
                                                    $varunit = $variantcode->stock_unit_of_measure_code;
                                                    $varquantity = $variantcode->quantity_per_unit;
                                                    $inventory = doubleval($item->qty) * doubleval($varquantity);


                                                        do {
                                                            try {
                                                                $stockkeeping = stockkeeping::orderBy('exprit_date', 'ASC')->where('product_no', '=', $item->product_no)->where('statuse', '=', 'open')->first();
                                                                $stinventory = $stockkeeping->inventory;
                                                                if (doubleval($stinventory) > doubleval($inventory)) {
                                                                    $product = productinsale::create([
                                                                        'document_no' => $serail_no,
                                                                        'boom_product_id' => $item->product_no,
                                                                        'product_no' => $item->product_no,
                                                                        'description' => $item->description,
                                                                        'unit_code' => $varunit,
                                                                        'inventory' => $inventory,
                                                                        'per_unit' => '1',
                                                                        'stock_id'=>$stockkeeping->id,
                                                                        'created_by' => $request['created_by'],
                                                                    ]);
                                                                    $stinventory = doubleval($stinventory) - doubleval($inventory);
                                                                    $stockkeeping->update(['inventory' => $stinventory]);
                                                                    $inventory = 0;
                                                                } else {
                                                                    $product = productinsale::create([
                                                                        'document_no' => $serail_no,
                                                                        'boom_product_id' => $item->product_no,
                                                                        'product_no' => $item->product_no,
                                                                        'description' => $item->description,
                                                                        'unit_code' => $varunit,
                                                                        'inventory' => $stinventory,
                                                                        'per_unit' => '1',
                                                                        'stock_id'=>$stockkeeping->id,
                                                                        'created_by' => $request['created_by'],
                                                                    ]);
                                                                    $stinventory = doubleval($inventory) - doubleval($stinventory);
                                                                    $stockkeeping->update(['inventory' => 0, 'statuse' => 'close']);
                                                                }
                                                            } catch (Exception $e) {}
                                                        } while ($inventory > 0);

                                                }
                                            }
                                        }
                                    }
                                   if($prescriptions->appointment_date =='') {
                                        $prescriptions->status = "close";
                                        $prescriptions->save();
                                      }else{
                                       $prescriptions->status = "waiting";
                                       $prescriptions->save();
                                     }
                                    if($prescriptions){
                                        $payment_amount = payment_amount::create([
                                            'document_no' => $serail_no,
                                            'document' => 'labo sale service Invoince',
                                            'document_type' => 'income',
                                            'decription' => 'labo sale service Invoince' . $serail_no,
                                            'totale_balanec' => $request->total_amount,
                                            'exchane_rate' =>   $request['ExchangeRate'],
                                            'currency_code' =>  $curencycode,
                                            'paymant_amount' => $request->total_amount,
                                            'statue' => 'open',
                                            'created_by' => $request['created_by'],
                                        ]);
                                    }
                                    if( $payment_amount){
                                        return ['statue' => true];
                                    }
                                }
                }  else {
                     return ['statue' => false];
                }
            }else{

            }
        } else {
            return ['statue' => false];
        }
    }
    public function index()
    {
        $curency = payment_amount::get();
        return $curency;
    }

    public function create(Request $request)
    {
        $exchangerate = paymentMethod::create([
            'paymentmethod_code' => $request['paymentmethod_code'],
            'paymentmethod' => $request['paymentmethod'],
            'description' => $request['description'],
            'statue' => $request['statue'],
            'inactived' => $request['inactived'],
            'created_by' => $request['created_by'],
        ]);
        if ($exchangerate) {
            return paymentMethod::where('type', '=', 'groupservice')->get();
        } else {
            return ['statue :' => "faile"];
        }
    }

    public function update($id, Request $request)
    {
        $exchangerate = paymentMethod::find($id);
        $exchangerate->paymentmethod_code = $request->paymentmethod_code;
        $exchangerate->paymentmethod = $request->paymentmethod;
        $exchangerate->description = $request->description;
        $exchangerate->statue = $request->statue;
        $exchangerate->inactived = $request->inactived;
        $exchangerate->updated_by = $request->updated_by;
        $exchangerate->save();
        if ($exchangerate) {
            return $exchangerate;
        } else {
            return ['statue :' => "faile"];
        }
    }
    public function destroy($id)
    {
        $curency = paymentMethod::find($id);
        $curency->delete();
        return paymentMethod::get();
    }

    public function reverseSaleProduct(Request $request){
        if($request->document_no != '' && $request->document_no != null){
            $paymentamount = payment_amount::where('document_no','=',$request->document_no)->first();
            if($paymentamount->statue != 'demo'){
                // update status for payment_amount

                $paymentamount->statue = "demo";
                $paymentamount->updated_by = $request->updated_by;
                $paymentamount->save();

                //get prescription data
                    $prescription = prescriptions::where('preid','=',$paymentamount->document_no)->first();
                    $prescription->status = "reverse";
                    $prescription->updated_by = $request->updated_by;
                    $prescription->save();

                //get all products that sell in a document
                $product_in_sale = productinsale::where('document_no','=',$paymentamount->document_no)->get();

               // return $product_in_sale;
                if(count($product_in_sale) > 0){
                    foreach($product_in_sale as $product){

                        //get product stock unit
                        $stock_unit = stockkeeping::orderBy('id','ASC')->where('product_no','=',$product->product_no)->where('statuse','=','open')->first();

                        //add back qty stock to stockeeping
                        $qty = doubleval($stock_unit->inventory) + doubleval($product->inventory);
                        $stock_unit->inventory = $qty;
                        $stock_unit->updated_by = $request->updated_by;
                        $stock_unit->save();

                        //delete product in sale
                        $product->delete();
                    }
                }
                //get data from sale
                $sale = Sale::where('document_no','=',$paymentamount->document_no)->first();
                //get saleline item
                $saleLine = Saleline::where('document_no','=',$paymentamount->document_no)->get();
                //loop to delete every item in a sale
                if(count($saleLine) > 0){
                    foreach($saleLine as $element){
                        // $element->delete();
                        $element->statue = "demo";
                        $element->updated_by = $request->updated_by;
                        $element->save();
                    }
                }
                //delete sale
                $sale->statue = "demo";
                $sale->updated_by = $request->updated_by;
                $sale->save();

                return ['status' => "Reverse successfully"];
            }else{
                return ['status' => "This has reversed already!"];
            }
        }
        else{
            return ['status'=> 'No data to reverse'];
        }

    }
    public function copyToAgain(Request $request){
        $items = Saleline::where('document_no','=',$request->document_no)->get();
        if($items){
            if(count($items) > 0){
                foreach($items as $el){
                   $data = pos_sale::create([
                        'document_no' => $el->document_no,
                        'product_no' => $el->product_no,
                        'description' => $el->description,
                        'unit_code' => $el->unit_code,
                        'unit_price' => $el->unit_price,
                        'inventory' => $el->inventory,
                        'total_amount' => $el->total_amount,
                        'curency_code' => $el->curency_code,
                        'statue' => 'open',
                        'created_by'  => $request->updated_by
                   ]);
                }
            return ["message" => "copy successfully"];
            }

        }else{
            return ["message" => "no data to copy"];
        }

    }
    public function getReversePatient($id){
        $patient = prescriptions::where('preid', '=', $id)->first();
        if($patient != null){
            return ['data'=>$patient];
        }else{
            return ['data' => "No data to return"];
        }

    }


}
