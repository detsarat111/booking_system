<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\payment_amount;
use App\Models\product;
use App\Models\pos_sale;
use App\Models\close_shift;
use App\Models\pos_closeshift;
use App\Models\closeshift_view;
use App\Models\StockOnHand;
use App\Models\User;
use App\Models\product_vavaincode_view;
use App\Models\saleuser;
use App\Models\expenses_type;
use App\Models\stockkeeping;
class Poscontroller extends Controller
{
    public function InposLine(Request $request){
            $pos_sale = pos_sale::where("product_no","=",$request->product_no)
                                    ->where("size","=",$request->size)
                                    ->where("model","=",$request->mood)
                                    ->where("sugar","=",$request->sugar)
                                    ->where("statue","=","open")
                                    ->where("created_by","=",$request->created_by)
                                    ->get();
            if(count($pos_sale) > 0 ){
                foreach ($pos_sale as $key) {
                    $val = floatval($key->inventory);
                    $val = $val + floatval($request->inventory);
                    $total = floatval($request->price) *  $val;
                    $key->inventory =$val;
                    $key->total_amount=$total;
                    $key->save();
                    if( $pos_sale){
                      $item = pos_sale::select('id','document_no','product_no','description','unit_code','inventory','total_amount','unit_price')->orderBy('id', 'DESC')->where('created_by', '=', $request->created_by)->where('statue', '=', 'open')->get();
                      return ['statue' => true, 'item' => $item];
                    }
                }
            }else {
                $pos_sale = pos_sale::create([
                    'product_no'        => $request['product_no'],
                    'description'       => $request['description'],
                    'unit_code'         => $request['variant_unit_of_measure_code'],
                    'unit_price'        => $request['price'],
                    'inventory'         => $request['inventory'],
                    'total_amount'      => $request['total_amount'],
                    'curency_code'      => $request['curency_code'],
                    'model'             => $request['mood'],
                    'size'              => $request['size'],
                    'sugar'             => $request['sugar'],
                    'statue'            => 'open',
                    'created_by'        => $request['created_by'],
                ]);
                $item = pos_sale::select('id','document_no','product_no','description','unit_code','inventory','total_amount','unit_price')->orderBy('id', 'DESC')->where('created_by', '=', $request->created_by)->where('statue', '=', 'open')->get();
                    return ['statue' => true, 'item' => $item];
            }

        
    }

    public function getposdata(Request $request){
        $pos_sale = pos_sale::orderBy('id', 'DESC')->where('created_by', '=', $request->created_by)->where('statue', '=', 'open')->get();
        return $pos_sale;
    }

    public function getposcloseshift($created_by,Request $request){
        $pos_sale = pos_sale::first();
        return $pos_sale;
    }


    public function deleteDatepos($id,Request $request){
            $pos_sale = pos_sale::find($id); 
            $pos_sale-> delete();
            return pos_sale::orderBy('id', 'DESC')->where('created_by', '=',$request->created_by)->where('statue', '=', 'open')->get();
    }
    public function cleardata(Request $request){
            $productbom = pos_sale::where('created_by', '=',$request->created_by)->where('statue', '=', 'open')->get();
            foreach ($productbom as $element) {
                $element-> delete();
            }
            return pos_sale::orderBy('id', 'DESC')->where('created_by', '=',$request->created_by)->where('statue', '=', 'open')->get();
    }
    public function update($id,Request $request)
    {      
            $pos_sale = pos_sale::find($id); 
            // return $pos_sale;
                      $pos_sale->inventory = $request->inventory;
                      $pos_sale->total_amount = $request->total_amount;
                      $pos_sale->save();
                      if($pos_sale){
                            return ['statue' => true];
                        }else{
                            return ['statue' => false,'inventory'=>$stock_onhand.$stock_uint];
                        } 
    }

     public function proserach(Request $request)
    {
        
        if($chheck != ""){ 
                $product= product::addSelect('id', 'product_no', 'product_barcode', 'description', 'description_2', 'image_url', 'stock_unit_of_measure_code', 'purche_unit_of_measure_code', 'bom_no', 'reorder_point', 'sup_code', 'brand_code', 'group_code', 'cat_code', 'variant_code', 'unit_price', 'inactived', 'is_deleted', 'created_by', 'updete_by', 'delete_by', 'created_at', 'updated_at')
                ->where('cat_code', '=',$request->cat_code)
                ->orwhere('description', 'LIKE', "%{$request->textsearch}%")
                ->orwhere('description_2', 'LIKE', "%{$request->textsearch}%")
                ->orwhere('product_barcode', 'LIKE', "%{$request->textsearch}%")
                ->orwhere('product_no', 'LIKE', "%{$request->textsearch}%")
                ->get();
                return $product; 
        }else{
            $product= product::addSelect('id', 'product_no', 'product_barcode', 'description', 'description_2', 'image_url', 'stock_unit_of_measure_code', 'purche_unit_of_measure_code', 'bom_no', 'reorder_point', 'sup_code', 'brand_code', 'group_code', 'cat_code', 'variant_code', 'unit_price', 'inactived', 'is_deleted', 'created_by', 'updete_by', 'delete_by', 'created_at', 'updated_at')
                ->orwhere('description', 'LIKE', "%{$request->textsearch}%")
                ->orwhere('description_2', 'LIKE', "%{$request->textsearch}%")
                ->orwhere('product_barcode', 'LIKE', "%{$request->textsearch}%")
                ->orwhere('product_no', 'LIKE', "%{$request->textsearch}%")
                ->get();
                return $product; 
        }
    }

      public function createnewsale(Request $request){
           $pos_sale = pos_sale::create([
            'description'    => $request['description'],
            'deposit'        => $request['deposit'],
            'deposit2'       => $request['deposit2'],
            'curency_code'   => $request['curency_code'],
            'curency_code2'  => $request['curency_code2'],
            'statue'         => 'open',
            'created_by'     => $request['created_by'],
        ]);
         return $pos_sale; 
    }
    public function getclose($id, Request $request){
        if($request->name == '' && $request->startDate == '' && $request->endDate == ''){
            $result = closeshift_view::orderBy('created_at','desc')->where('user_code','=',$id)
                                 ->paginate(15);
            $print = closeshift_view::orderBy('created_at','desc')->where('user_code','=',$id)
                                    ->get();
            return ['data' => $result, 'print' => $print];
        }
        
        else if($request->name != ''){
            $result = closeshift_view::orderBy('created_at','desc')->where('user_code','=',$id)
                                    ->whereIn('id', closeshift_view::select('id')
                                    ->where('user_name','LIKE','%'.$request->name.'%')
                                    ->orwhere('deposit','LIKE','%'.$request->name.'%')
                                    ->orwhere('deposit2','LIKE','%'.$request->name.'%')
                                    ->orwhere('exchane_rate','LIKE','%'.$request->name.'%')
                                    ->orwhere('total_amount','LIKE','%'.$request->name.'%')
                                    ->orwhere('description','LIKE','%'.$request->name.'%')
                                    ->get())
                                    ->paginate(15);
            $print = closeshift_view::orderBy('created_at','desc')->where('user_code','=',$id)
                                        ->whereIn('id', closeshift_view::select('id')
                                        ->where('user_name','LIKE','%'.$request->name.'%')
                                        ->orwhere('deposit','LIKE','%'.$request->name.'%')
                                        ->orwhere('deposit2','LIKE','%'.$request->name.'%')
                                        ->orwhere('exchane_rate','LIKE','%'.$request->name.'%')
                                        ->orwhere('total_amount','LIKE','%'.$request->name.'%')
                                        ->orwhere('description','LIKE','%'.$request->name.'%')
                                        ->get())
                                        ->get();
            return ['data' => $result, 'print' => $print];
        }
        else if($request->startDate != '' && $request->endDate != ''){
            $result = closeshift_view::orderBy('created_at','desc')->where('user_code','=',$id)
                                    ->whereIn('id', closeshift_view::select('id')
                                    ->whereDate('created_at','>=',$request->startDate)
                                    ->whereDate('created_at','<=',$request->endDate)
                                    ->get())
                                    ->paginate(15);
            $print = closeshift_view::orderBy('created_at','desc')->where('user_code','=',$id)
                                        ->whereIn('id', closeshift_view::select('id')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        ->get())
                                        ->get();
            return ['data' => $result, 'print' => $print];
        }

    }

    // close shift report 
    public function getsht($id){
        $user = User::find($id);
        if($user->role == 'Admin' ||$user->role == 'System'){
            $result = closeshift_view::paginate(15);
            return ['status'=>true,'data'=>$result];
        }else{
            return ['status'=>false,'message'=>"You have not permission!"];
        }
    }
    public function shsht(Request $request,$id){
        $user = User::find($id);
        if($user->role == 'Admin' || $user->role == 'System'){
            if($request->name == null){
                $result = closeshift_view::whereDate('created_at','>=',$request->start)
                                            ->whereDate('created_at','<=',$request->end)
                                            ->paginate(15);
            }
            else{
                $result = closeshift_view::whereDate('created_at','>=',$request->start)
                                          ->whereDate('created_at','<=', $request->end)
                                            ->where('user_name','LIKE','%'.$request->name.'%')
                                            ->paginate(15);
            }
            return ['status'=>true,'data'=>$result];
        }else{
            return ['status'=>false,'message'=>"You have not permission!"];
        }
    }

    // end close shift report 
    public function searchclose(Request $request,$id){
        if($request->name != ''){
            $result = closeshift_view::where('user_code','=',$id)
                                    ->whereIn('id', closeshift_view::select('id')
                                    ->where('user_name','LIKE','%'.$request->name.'%')
                                    ->orwhere('deposit','LIKE','%'.$request->name.'%')
                                    ->orwhere('deposit2','LIKE','%'.$request->name.'%')
                                    ->orwhere('exchane_rate','LIKE','%'.$request->name.'%')
                                    ->orwhere('total_amount','LIKE','%'.$request->name.'%')
                                    ->orwhere('description','LIKE','%'.$request->name.'%')
                                    ->get())
                                    ->paginate(2);
        return $result;
        }
        if($request->startDate != '' && $request->endDate != ''){
            $result = closeshift_view::where('user_code','=',$id)
                                    ->whereIn('id', closeshift_view::select('id')
                                    ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                    ->get())
                                    ->paginate(2);
            return $result;
        }
    }
    public function salereport($id){
        $user = User::find($id);
        if($user->role == 'Admin' ||$user->role == 'System'){
            $sale = saleuser::orderBy('id', 'DESC')
                            ->paginate(15);
            return ['status'=>true,'data'=>$sale];
        }else{
            return ['status'=>false,'message'=>"You have not permission!"];
        }
       
    }
    public function savePostItem($id){
        $data = pos_sale::where('created_by','=',$id)
                            ->where('statue','=','open')
                            ->get();
        foreach ($data as $el) {
            $el->statue = 'save';
            $el->updated_by = $id;
            $el->save();
        }
        if(count($data) != null){
            return ['status'=>true, 'message'=> 'Save successfully'];
        }else{
            return ['status'=>false, 'message'=> 'Failed save'];
        }
    }
    public function reverseItem($id){
        $data = pos_sale::where('created_by','=',$id)
                        ->where('statue','=','open')
                        ->get();
        if(count($data) == 0){
            $reverses = pos_sale::where('created_by','=',$id)
                                    ->where('statue','=','save')
                                    ->get();
            foreach($reverses  as $el){
                $el->statue = 'open';
                $el->updated_by = $id;
                $el->save();
            }
            if($reverses){
                return ['status'=> true, 'message' => 'Reverse successfully'];
            }else{
                return ['status' => false, 'message' => "Can't reverse item." ];
            }

        }else{
            return ['status' => false, 'message' => "Can't reverse. Please sell item first"];
        }
    }
    public function isHaveItemtoReverse($id){
        $item = pos_sale::where('created_by','=',$id)
                        ->where('statue','=','save')
                        ->get();
        if(count($item) != 0){
            return ['status'=>true, 'message'=> 'Have item to reverse.'];
        }else{
            return ['status'=>false, 'message' => "Haven't item to reverse."];
        }
    }
    public function getexpenType(){
        $result = expenses_type::get();
        return ['data'=>$result, 'message'=>'data founded.'];
    }
}