<?php

namespace App\Http\Controllers;

use App\Models\category;
use Illuminate\Http\Request;

class CategorysController extends Controller
{
    public function index(Request $request)
    {
        $category = category::where('cat_name', 'LIKE',$request->search.'%')->where('inactived', '!=', 'Yes')
            ->orderBy('id', 'desc')->paginate(15);

            $categorys = category::where('cat_name', 'LIKE',$request->search.'%')->where('inactived', '!=', 'Yes')
            ->orderBy('id', 'desc')->get();
        return ['category'=>$category, 'print'=>$categorys];
    }

    public function store(Request $request)
    {
        $request->validate([
            'cat_name' => 'required',
        ]);

        return category::create([
            'cat_name' => $request['cat_name'],
            'cat_name_2' => $request['cat_name_2'],
            'inactived' => 'No',
            'created_by' => $request['created_by'],
        ]);
    }

    public function update(Request $request)
    {
        $category = category::where('id', '=', $request->id)->first();
        // $category = category::find($request->id);
        $category->cat_name = $request->cat_name;
        $category->cat_name_2 = $request->cat_name_2;
        $category->inactived = $request->inactived;
        $category->updated_by = $request->updated_by;
        $category->save();

        return $category;
    }

    public function getCatName()
    {
        $category = category::where('inactived', '!=', 'Yes')->get();

        return $category;
    }

    public function storeExcel(Request $request)
    {
        $category = category::create([
            'cat_code' => $request->cat_code,
            'cat_name' => $request->cat_name,
            'cat_name_2' => $request->cat_name_2,
            'inactived' => $request->inactived,
            'is_deleted' => '0',
            'created_by' => 'Chhin Pov',
        ]);
        if ($category) {
            return category::get()->all();
        } else {
            return ['statue :' => 'faile'];
        }
    }
    public function searchCategory($search){
        $data = category::where('inactived','!=','Yes')
            ->where('cat_name','LIKE','%'.$search.'%')->get();
        return $data;
    }
}
