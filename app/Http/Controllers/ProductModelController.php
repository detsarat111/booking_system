<?php

namespace App\Http\Controllers;

use App\Models\productgroup;
use Illuminate\Http\Request;

class ProductModelController extends Controller
{
    public function index()
    {
        $product = productgroup::where('type', '=', 'Model')->where('inactived', '!=', 'Yes')->orderBy('id', 'desc')->paginate(15);
        $products = productgroup::where('type', '=', 'Model')->where('inactived', '!=', 'Yes')->orderBy('id', 'desc')->get();

        return ['group' => $product, 'print' => $products];
    }

    public function searchGroup(Request $request)
    {
        $productgroup = productgroup::where('type','=','Model')
                                    ->where('inactived','=','No')
                                    ->where('group_name', 'LIKE',$request->search.'%')
                                    ->orderBy('id', 'desc')->paginate(15);

        return $productgroup;
    }

    public function getdatagroup()
    {
        $productgroup = productgroup::where('type', '=', 'Model')->get();

        return $productgroup;
    }

    public function create(Request $request)
    {
        $request->validate([
            'group_name' => 'required',
        ]);
        $productgroup = productgroup::create([
            'group_code' => $request['group_name'],
            'group_name' => $request['group_name'],
            'image_url' => $request['image_url'],
            'group_name_2' => $request['group_name_2'],
            'brand_code' => $request['brand_code'],
            'type' => 'Model',
            'cat_code' => $request['cat_code'],
            'inactived' => 'No',
            'created_by' => $request['created_by'],
        ]);
        if ($productgroup) {
            return ['statue :' => 'succese'];
        } else {
            return ['statue :' => 'faile'];
        }
    }

    // Group service
    public function Groupservice()
    {
        $productgroup = productgroup::where('type', '=', 'Model')->where('inactived', '!=', 'Yes')
        ->orderBy('group_code')->paginate(15);
        $productgroups = productgroup::where('type', '=', 'Model')->where('inactived', '!=', 'Yes')
        ->orderBy('group_code')->get();
        return ['group'=>$productgroup, 'print'=>$productgroups];
    }

    public function SelectGroupservice()
    {
        $productgroup = productgroup::where('type', '=', 'Model')->where('inactived','!=','Yes')->get();

        return $productgroup;
    }

    public function createGroupservice(Request $request)
    {
        $request->validate([
            'group_name' => 'required',
        ]);
        $productgroup = productgroup::create([
            'group_code' => $request['group_name'],
            'group_name' => $request['group_name'],
            'image_url' => $request['image_url'],
            'group_name_2' => $request['group_name_2'],
            'brand_code' => $request['brand_code'],
            'type' => 'Model',
            'cat_code' => $request['cat_code'],
            'inactived' => "No",
            'created_by' => $request['created_by'],
        ]);

        return $productgroup;
        // if($productgroup){
        //   return ['statue :'=>"succese"];
        // }else{
        //   return ['statue :'=>"faile"];
        // }
    }

    // end Group service

    public function update($id, Request $request)
    {
        $productgroup = productgroup::find($id);
        $productgroup->group_code = $request->group_name;
        $productgroup->group_name = $request->group_name;
        $productgroup->image_url = $request->image_url;
        $productgroup->group_name_2 = $request->group_name_2;
        $productgroup->brand_code = $request->brand_code;
        $productgroup->inactived = $request->inactived;
        $productgroup->cat_code = $request->cat_code;
        $productgroup->updated_by = $request->updated_by;
        $productgroup->save();
        if ($productgroup) {
            return ['statue :' => 'Succesfull'];
        } else {
            return ['statue :' => 'faile '];
        }
    }

    
}
