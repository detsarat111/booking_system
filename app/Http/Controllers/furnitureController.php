<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\rooms;
use App\Models\roomstype;
use App\Models\furnitures;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class furnitureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showfurniture()
    {
        $furniture = furnitures::get();
        return $furniture;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    //  TBALE RURNITURE //

        // how to create createroom //


        public function makeDirectory($directory)
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
    }
    public function directory()
    {
        return $dir = 'img' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
    }
    public function uploadPhoto(Request $request)
    {
        $dir = $this->directory();
        $file = $request->image;
        $this->makeDirectory($dir);
        $photo = time() . '.' . explode(';', explode('/', $file)[1])[0];
        Image::make($file)->save(public_path($dir) . $photo);

        return $photo;
    }



    public function createfurniture(Request $request)
    {
        $requestData = $request->all();
        $validator   = Validator::make($requestData, [
            'furniture'         => 'required',
            'description'       => 'required',
            'image'             => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()   ->json([
                'errors'        => $validator->errors(),
            ], 422);
        }


        $dir = $this->directory();
        $file = $request->image;
        $this->makeDirectory($dir);
        $photo = time() . '.' . explode(';', explode('/', $file)[1])[0];
        Image::make($file)->save(public_path($dir) . $photo);


        $furniture= furnitures::create([
            'furniture'         => $request->furniture,
            'description'       => $request->description,
            'image'            => $photo,
           
        ]);
        if($furniture){
             $furniture = furnitures::get();
             return $furniture;
        }
        else{
            return 'false';
        }
    }

    // how to create update furniture
    public function updatefurniture($id, Request $request){

        $requestData = $request->all();
      
        $validator   = Validator::make($requestData, [
            'furniture'         => 'required',
            'description'       => 'required',
            'image'             => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()   ->json([
                'errors'        => $validator->errors(),
            ], 422);
        }
        $data = furnitures::find($id);
        if($data != null){
            $data->furniture         = $request->furniture;
            $data->description       = $request->description;
            $data->image             = $request->image;
            $data->save();
            if($data){
                return "Update successfully";
            }
        }else {
            return "No data to update";
        }

        


    }

    // how to create delete furniture //
    public function deletefurniture($id){
        $data = furnitures::find($id);
        if($data != null){
            $data->delete();
            return "Deleted successfully";
        }else {
            return "No data to delete";
        }
    }

    // how to create update furniture //

    public function searchfurniture(Request $request)
    {
        $furniture = furnitures::where("furniture", 'LIKE', "%$request->furniture%")
                        ->where("description", 'LIKE', "%$request->description%")
                        ->get();
        if ($furniture) {
            return $furniture;
        } else {
            return ['statue :' => "Note Date"];
        }
    }

    //  TBALE RURNITURE //


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    
}
