<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomFurnitureView extends Model
{
    use HasFactory;
    protected $table = 'furniture_view';
    protected $fillable = [
        'id',
        'room_no',
        'furniture_no',
        'qty',
        'status',
        'furniture',
        'description',
        'image',
        'created_by',
        'updated_by'
    ];
}
