<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $table = 'sale';
    protected $fillable = [
        'id',
        'document_no',
        'document_type',
        'closeshift_id',
        'description',
        'total_amount_usd',
        'total_amount',
        'change_amount',
        'paymeny_method',
        'change_amount_usd',
        'type',
        'amoun',
        'total_receipts',
        'total_receipt_usd',
        'discount',
        'curency_code',
        'exchane_rate',
        'statue',
        'created_by',
        'updated_by',
   ];
}