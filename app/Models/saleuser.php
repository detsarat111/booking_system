<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class saleuser extends Model
{
    use HasFactory;
    protected $table = "saleuser";
    protected $fillable = [
        'id',
        'document_no',
        'document_type',
        'closeshift_id',
        'description',
        'total_amount_usd',
        'total_amount',
        'change_amount',
        'paymeny_method',
        'change_amount_usd',
        'type',
        'total_receipts',
        'total_receipt_usd',
        'discount',
        'curency_code',
        'exchane_rate',
        'statue',
        'created_by',
        'updated_by',
        'user_name',
        'amoun',
        'created_at',
    ];
}