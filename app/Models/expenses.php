<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class expenses extends Model
{
    use HasFactory;
    protected $table ="expenses";
    protected $fillable = [
        'id',
        'document_no',
        'document',
        'description',
        'Payment_method',
        'totat_exspan',
        'statue',
        'exspan_date',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
