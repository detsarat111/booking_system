<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class roomfurniture extends Model
{
    use HasFactory;
    protected $table = 'room_furniture';
    protected $fillable = [
        'id',
        'room_no',
        'furniture_no',
        'qty',
        'status',
        'created_by',
        'updated_by'
    ];
}
