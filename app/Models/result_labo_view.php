<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class result_labo_view extends Model
{
    use HasFactory;
    protected $table = "result_labo_view";
    protected $fillable = [
        'id',
        'stock_unit_of_measure_code',
        'group_code',
        'labId',
        'product_no',
        'product_type',
        'description',
        'test_type',
        'normal_value',
        'test_value',
        'unit_price',
        'status',
        'remark',
        'created_by',
        'updated_by'
    ];
}
