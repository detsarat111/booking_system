<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IncomeReportView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW income_report_view AS
        (
            SELECT
            ps.*, pk.unit_price as buy_price,
        s.paymeny_method, s.type,
               COALESCE (
                        (
                          SELECT
                            COALESCE (unit_price, 0) AS sel_price
                          FROM
                            product_variant_code
                          WHERE
                            status = 'stock'
                          AND product_no = ps.product_no
                        
                        ),
                        0
                      ) AS sel_price
        
        
        FROM
            product_in_sale ps
        INNER JOIN product_stock_keeping_units pk
         ON ps.stock_id = pk.id
        
        INNER JOIN sale s ON s.document_no = ps.document_no
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS income_report_view');
    }
}
