<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StockForSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    DB::statement("
        CREATE VIEW stock_sales AS (
       SELECT
        pu.id,
            P.product_no,
            P.description,
            P.reorder_point,
        P.stock_unit_of_measure_code as stock_uint,
        COALESCE (P.unit_price, 0) AS unit_price,
            COALESCE (SUM(pu.inventory), 0) AS stock_onhand,
        COALESCE (SUM(pu.inventory), 0)*COALESCE(pu.unit_price,0) AS stock_aoumt,
        pu.curency_code,
        COALESCE(to_days(pu.exprit_date) - to_days(CURRENT_TIMESTAMP()),0) AS countday_exprit,
        pu.exprit_date
        FROM
            product P
        LEFT OUTER JOIN product_stock_keeping_units pu ON P.product_no = pu.product_no
        WHERE
            P.type = 'Products' and pu.statuse = 'open' 
        GROUP BY
            P.product_no,
            P.product_barcode,
            P.description,
            P.stock_unit_of_measure_code,
            P.reorder_point,
            P.unit_price,
            pu.exprit_date,
            pu.unit_price,
            pu.curency_code,
            pu.id
        ORDER BY countday_exprit,pu.id ASC
         )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         DB::statement("DROP VIEW IF EXISTS stock_sales");
    }
}