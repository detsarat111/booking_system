require("./bootstrap");
require("alpinejs");
import Alpine from "alpinejs";
window.Alpine = Alpine;
import "vue-search-select/dist/VueSearchSelect.css"
import { createApp } from "vue";
import router from "./router/router";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import axios from "axios";
import App from "./components/App.vue";
import Brands from "./components/Brands/Brands.vue";
import Product from "./components/Product/product.vue";
import Viewproduct from "./components/Product/view-product.vue";
import Category from "./components/Category/Category.vue";
import Supplies from "./components/Supplies/Supplies.vue";
import Group from "./components/Group/Product_group.vue";
import Serailcode from "./components/Serailcode/serailcode.vue";
import Purchase from "./components/Purchase/purchase-order.vue";
import Purchasview from "./components/Purchase/view-puchese.vue";
import MenuProductSetting from "./components/MenuProductsetup.vue";
import MenuPurchase from "./components/MenuPurchase.vue";
import MenuStock from "./components/MenuStock.vue";
import Stockalert from "./components/StockAlert/stockalert.vue";
import MenuClinic from "./components/MenuClinic.vue";
import MenuMedical from "./components/MenuMedical.vue";
import Receptorder from "./components/Recept/Recept-order.vue";
import viewRecept from "./components/Recept/view-Recept.vue";
import unitcode from "./components/unitcode/unitcode.vue";
import StockTransection from "./components/StockTransection/stock-transection.vue";
import Currency from "./components/Currency/currency.vue";
import ExchangeRate from "./components/ExchangeRate/ExchangeRate.vue";
import StockCount from "./components/StockCount/stock-count.vue";
import paymenMethod from "./components/PaymentMethod/paymenMethod.vue";
import paymen from "./components/Payment/paymen.vue";
import paymenview from "./components/Payment/paymen_view.vue";
import Laboratory from "./components/Prescriptions/prescriptions.vue";
import Labolap from "./components/Laboratory/Laboratory.vue";
import convertoInvoine from "./components/Prescriptions/convertoInvoine.vue";
import test from "./components/Laboratory/Test.vue";
import Viewlaboratory from "./components/viewPrescription/Viewprescription.vue";
import testserviceview from "./components/Laboratory/TestServiceView.vue";
import viewlabolap from "./components/Laboratory/Viewlaboratory.vue";
import addTestLabo from "./components/Laboratory/AddTestLabo.vue";
import Employee from "./components/Employee/employee.vue";
import patientRegister from "./components/Register/patientRegister.vue";
import Vue2Filters from "vue2-filters";
import VueHtmlToPaper from "vue-html-to-paper";
import print from 'vue3-print-nb'
import TestInvoice from "./components/TestInvoice/testinvoice.vue";
import detailshif from "./components/Detailcloseshif/detailshif.vue";
import saledetail from "./components/Saledetail/saledetail.vue";
import role from "./components/Role/role.vue";
import stockonhand_date from "./components/Expire/expire.vue";
import ex_alert from "./components/Exlert/ex_alert.vue";
import prescription from "./components/Diagnosis/Diagnosis.vue";
import prescriptionview from "./components/PrescriptionView/prescriptionview.vue";
import viewall from "./components/Viewall/viewall.vue";
import report from "./components/Report/report.vue";
import salereport from "./components/Report/salereport.vue";
import incomepen from "./components/Report/income-expen.vue";
import saleitem from "./components/Report/saleitem.vue";
import stockinven from "./components/Report/stockinven.vue";
import incomereport from "./components/Report/sale-product.vue";
import closereport from "./components/Report/closereport.vue";
import expensesreport from "./components/Expenses/expenses.vue";
import expenses from "./components/Expensesview/expensesview.vue";
import expenses1 from "./components/Expenses1/expenses1.vue";
import income from "./components/Income/income.vue";
import income1 from "./components/Income1/income1.vue";
import labototal from "./components/Labototal/labototal.vue";
import groupService from "./components/Groupservice/groupService.vue";
import calendar from "./components/Calendar/calendar.vue";
import calendarView from "./components/Calendar/calendarView.vue";
import Appointment from "./components/Appointment/Appointment.vue";
import booking from "./components/booking/booking.vue";
import room_type from "./components/room_type/room_type.vue";
import room from "./components/room_type/room.vue";
import room_furniture from "./components/room_type/room_furniture.vue";
import booking_line from "./components/room_type/booking_line.vue";
import room_price from "./components/room_type/room_price.vue";
import bookingone from "./components/room_type/bookingone.vue";
import furniture from "./components/room_type/furniture.vue";
import testone from "./components/room_type/testone.vue";
import product_model from "./components/Model/Product_model.vue";
import pro_unittype from "./components/UnitType/Product_UnitType.vue";
import view_room from "./components/room_type/view_room.vue";


window.value = "Menu";
const app = createApp({
    components: {
        App,
        Brands,
        Product,
        Viewproduct,
        Category,
        Supplies,
        Group,
        Serailcode,
        Purchase,
        Purchasview,
        MenuProductSetting,
        MenuPurchase,
        MenuStock,
        Stockalert,
        MenuClinic,
        MenuMedical,
        Receptorder,
        viewRecept,
        unitcode,
        StockTransection,
        Currency,
        ExchangeRate,
        StockCount,
        paymenMethod,
        paymen,
        paymenview,
        Laboratory,
        convertoInvoine,
        Labolap,
        test,
        addTestLabo,
        Employee,
        patientRegister,
        Viewlaboratory,
        testserviceview,
        TestInvoice,
        viewlabolap,
        detailshif,
        saledetail,
        role,
        prescription,
        stockonhand_date,
        ex_alert,
        prescriptionview,
        viewall,
        report,
        salereport,
        saleitem,
        closereport,
        expensesreport,
        expenses1,
        income,
        income1,
        labototal,
        groupService,
        expenses,
        stockinven,
        incomereport,
        incomepen,
        calendar,
        calendarView,
        Appointment,
        booking,
        room_type,
        room,
        room_furniture,
        booking_line,
        room_price,
        bookingone,
        furniture,
        testone,
        product_model,
        pro_unittype,
        view_room,
    },
});
Alpine.start();
app.provide("headingMain", window.value);
app.use(router, axios, Vue2Filters, VueSweetalert2, VueHtmlToPaper,print);
app.mount("#app");
